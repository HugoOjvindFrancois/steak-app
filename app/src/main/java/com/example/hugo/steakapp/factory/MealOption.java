package com.example.hugo.steakapp.factory;

import com.example.hugo.steakapp.model.Meal;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Hugo on 23/02/2017.
 */

class MealOption {

    private Meal meal = new Meal();

    public MealOption title(String title){
        meal.setTitle(title);
        return this;
    }

    public MealOption color(float color){
        meal.setColor(color);
        return this;
    }

    public MealOption description(String description){
        meal.setDescription(description);
        return this;
    }

    public MealOption coord(LatLng coord){
        meal.setCoord(coord);
        return this;
    }

}
