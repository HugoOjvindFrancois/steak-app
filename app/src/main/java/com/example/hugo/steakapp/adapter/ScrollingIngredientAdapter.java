package com.example.hugo.steakapp.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hugo.steakapp.R;
import com.example.hugo.steakapp.dummy.Ingredient;

import java.util.List;

/**
 * Created Hugo on 3/24/2017.
 */

public class ScrollingIngredientAdapter extends BaseAdapter {

    private List<Ingredient> ingredients;
    private LayoutInflater inflater;

    public ScrollingIngredientAdapter(Context context, List<Ingredient> list) {
        this.ingredients = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout;

        if (convertView == null) {
            layout = (LinearLayout) inflater.inflate(R.layout.ingredient_scrolling, parent, false);
        } else {
            layout = (LinearLayout) convertView;
        }


        TextView name = (TextView) layout.findViewById(R.id.scrolling_ingredient_name);
        TextView quantity = (TextView) layout.findViewById(R.id.scrolling_ingredient_quantity);

        name.setText(ingredients.get(position).name);
        quantity.setText(ingredients.get(position).quantity + "");

        name.setTag(position);
        quantity.setTag(position);

        return layout;
    }

}
