package com.example.hugo.steakapp.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

/**
 * Created by Hugo on 23/02/2017.
 */

public class User implements Parcelable {

    public static User instance = new User();

    private String mail;
    private String pwd;
    private String accountType;
    private Uri imageURI;
    private Uri imageBackup;
    private String phoneNumber;

    private User(){}

    public User(String email, String password, String phoneNumber){
        this.mail = email;
        this.pwd = password;
        this.phoneNumber = phoneNumber;
    }

    protected User(Parcel in) {
        this.mail = in.readString();
        this.pwd = in.readString();
        this.accountType = in.readString();
        this.imageURI = in.readParcelable(Uri.class.getClassLoader());
        this.phoneNumber = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mail);
        dest.writeString(this.pwd);
        dest.writeString(this.accountType);
        dest.writeParcelable(this.imageURI,flags);
        dest.writeString(this.phoneNumber);
    }

    public String getEmail(){
        return this.mail;
    }

    public void setMail(String mail){
        this.mail = mail;
    }

    public String getDisplayName(Context context){
        SharedPreferences generalSettings = PreferenceManager.getDefaultSharedPreferences(context);
        return generalSettings.getString("profile_display_name", "John Doe");
    }

    public void setPassword(String password){
        this.pwd = password;
    }

    public String getPassword(){
        return this.pwd;
    }

    public String getAccountType(){
        return this.accountType;
    }

    public void setAccountType(String accountType){
        this.accountType = accountType;
    }

    public void setImageURI(Uri imageURI) {
        if(imageBackup == null)
            imageBackup = imageURI;
        this.imageURI = imageURI;
    }

    public Uri getImageURI() {
        return imageURI;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
