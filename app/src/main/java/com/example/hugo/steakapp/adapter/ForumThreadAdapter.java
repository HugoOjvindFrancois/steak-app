package com.example.hugo.steakapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hugo.steakapp.R;
import com.example.hugo.steakapp.dummy.ForumThread;

import java.util.List;

/**
 * Created by LORD_OF_COMPSCI on 3/10/2017.
 */

public class ForumThreadAdapter extends BaseAdapter {

    private List<ForumThread> forumThreadList;

    private LayoutInflater inflater;

    public ForumThreadAdapter(List<ForumThread> list){
        this.forumThreadList = list;
    }

    @Override
    public int getCount() {
        return forumThreadList.size();
    }

    @Override
    public Object getItem(int position) {
        return forumThreadList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout;

        layout = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.forum_thread_layout, parent, false);

        TextView name = (TextView) layout.findViewById(R.id.forum_thread_name);

        name.setText(forumThreadList.get(position).name);

        return layout;
    }
}
