package com.example.hugo.steakapp;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.example.hugo.steakapp.LoginActivity;
import com.example.hugo.steakapp.model.User;
import com.example.hugo.steakapp.transform.CircleTransform;
import com.example.hugo.steakapp.util.MD5Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;

/**
 * Created by LORD_OF_COMPSCI on 3/13/2017.
 */

public class AppWidgetLogIn extends android.appwidget.AppWidgetProvider {

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int appWidgetId : appWidgetIds) {
            // Create an Intent to launch ExampleActivity
            Intent intent = new Intent(context, LoginActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);


            // Get the layout for the App Widget and attach an on-click listener
            // to the button
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            views.setOnClickPendingIntent(R.id.button3, pendingIntent);

            String displayName = User.instance.getDisplayName(context);
            views.setTextViewText(R.id.textView,"Hello, " + displayName);
            //views.setImageViewUri(R.id.imageView,User.instance.getImageURI());

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }



}
