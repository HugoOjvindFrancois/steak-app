package com.example.hugo.steakapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hugo.steakapp.R;
import com.example.hugo.steakapp.dummy.Ingredient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LORD_OF_COMPSCI on 3/7/2017.
 */

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.IngredientViewHolder> {

    private List<Ingredient> ingredients;
    private LayoutInflater inflater;
    private Context context;

    public IngredientAdapter(Context context, List<Ingredient> ingredients){
        this.ingredients = ingredients;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public IngredientViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = inflater.inflate(R.layout.meal_ingredient_layout, viewGroup, false);
        return new IngredientViewHolder(v);
    }

    @Override
    public void onBindViewHolder(IngredientViewHolder personViewHolder, int i) {
        personViewHolder.name.setText(ingredients.get(i).name);
        personViewHolder.imageAdd.setImageResource(ingredients.get(i).imageIdAdd);
        personViewHolder.imageRemove.setImageResource(ingredients.get(i).imageIdRemove);
        personViewHolder.imageDelete.setImageResource(ingredients.get(i).imageIdDelete);
        personViewHolder.imageAdd.setColorFilter(Color.parseColor("#a8a8a8"), PorterDuff.Mode.SRC_IN);
        personViewHolder.imageRemove.setColorFilter(Color.parseColor("#a8a8a8"), PorterDuff.Mode.SRC_IN);
        personViewHolder.imageDelete.setColorFilter(Color.parseColor("#a8a8a8"), PorterDuff.Mode.SRC_IN);
        personViewHolder.quantity.setText("" + ingredients.get(i).quantity);

        personViewHolder.name.setTag(i);
        personViewHolder.imageAdd.setTag(i);
        personViewHolder.imageRemove.setTag(i);
        personViewHolder.imageDelete.setTag(i);
        personViewHolder.quantity.setTag(i);

        personViewHolder.imageAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer position = (Integer)v.getTag();
                Ingredient i = ingredients.get(position);
                i.setQuantity(i.getQuantity() + 1);
                notifyItemChanged(position);
            }
        });

        personViewHolder.imageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer position = (Integer)v.getTag();
                Ingredient i = ingredients.get(position);
                i.setQuantity(i.getQuantity() > 1 ? i.getQuantity() - 1 : 1);
                notifyItemChanged(position);
            }
        });

        personViewHolder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer position = (Integer)v.getTag();
                Ingredient i = ingredients.get(position);
                ingredients.remove(i);
                notifyItemRemoved(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ingredients.size();
    }

    static class IngredientViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView imageAdd;
        ImageView imageRemove;
        ImageView imageDelete;
        TextView quantity;

        IngredientViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.ingredient_name);
            imageAdd = (ImageView)itemView.findViewById(R.id.ingredient_image_add);
            imageRemove = (ImageView)itemView.findViewById(R.id.ingredient_image_remove);
            imageDelete = (ImageView)itemView.findViewById(R.id.ingredient_image_delete);
            quantity = (TextView)itemView.findViewById(R.id.ingredient_quantity);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void addItem(Ingredient dataObj, int index) {
        ingredients.add(dataObj);
        notifyItemInserted(index);
    }

    public Ingredient get(int position){
        return ingredients.get(position);
    }

}
