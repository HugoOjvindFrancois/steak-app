package com.example.hugo.steakapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.hugo.steakapp.dummy.Ingredient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Meal implements Parcelable {

    private String title;
    private User host;
    private LatLng coord;
    private String description;
    private float color;
    private Calendar date;
    private List<Ingredient> list;
    private int people;

    public Meal(){}

    public Meal(String title, User host, LatLng coord, String description, float color, int year, int month, int day, int hour, int minute, List<Ingredient> list, int people){
        Calendar date = Calendar.getInstance(Locale.getDefault());
        date.set(year,month,day,hour,minute);
        this.color = color;
        this.coord = coord;
        this.description = description;
        this.title = title;
        this.date = date;
        this.host = host;
        this.list = list;
    }


    protected Meal(Parcel in) {
        this.title = in.readString();
        this.host = in.readParcelable(User.class.getClassLoader());
        this.coord = in.readParcelable(LatLng.class.getClassLoader());
        this.description = in.readString();
        this.color = in.readFloat();
        int year = in.readInt();
        int month = in.readInt();
        int day = in.readInt();
        int hour = in.readInt();
        int minute = in.readInt();
        this.date = Calendar.getInstance();
        date.set(year,month,day,hour,minute);
        this.list = new ArrayList<>();
        int size = in.readInt();
        for (int i = 0; i < size; i++)
            list.add((Ingredient) in.readParcelable(Ingredient.class.getClassLoader()));
        this.people = in.readInt();
    }
    public static final Creator<Meal> CREATOR = new Creator<Meal>() {
        @Override
        public Meal createFromParcel(Parcel in) {
            return new Meal(in);
        }

        @Override
        public Meal[] newArray(int size) {
            return new Meal[size];
        }
    };

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeParcelable(this.host,flags);
        dest.writeParcelable(this.coord,flags);
        dest.writeString(this.description);
        dest.writeFloat(this.color);
        dest.writeInt(this.date.get(Calendar.YEAR));
        dest.writeInt(this.date.get(Calendar.MONTH));
        dest.writeInt(this.date.get(Calendar.DAY_OF_MONTH));
        dest.writeInt(this.date.get(Calendar.HOUR_OF_DAY));
        dest.writeInt(this.date.get(Calendar.MINUTE));
        dest.writeInt(this.list.size());
        for ( Ingredient i : this.list )
            dest.writeParcelable(i,flags);
        dest.writeInt(this.people);
    }

    public void setColor(float color) {
        this.color = color;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCoord(LatLng coord) {
        this.coord = coord;
    }

    public void setdate(Calendar date){
        this.date = date;
    }

    public Calendar getDate(){
        return this.date;
    }

    public LatLng getCoord() {
        return coord;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public float getColor() {
        return color;
    }

    public User getHost(){
        return this.host;
    }

    public List<Ingredient> getList(){return this.list;}

    public void setHost(User user){
        this.host = host;
    }

    public int getPeople() {
        return people;
    }

    public void setPeople(int people) {
        this.people = people;
    }
}
