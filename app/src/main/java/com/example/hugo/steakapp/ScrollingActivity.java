package com.example.hugo.steakapp;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hugo.steakapp.adapter.ForumThreadAdapter;
import com.example.hugo.steakapp.adapter.ScrollingIngredientAdapter;
import com.example.hugo.steakapp.dummy.ForumThread;
import com.example.hugo.steakapp.dummy.Ingredient;
import com.example.hugo.steakapp.model.Meal;
import com.example.hugo.steakapp.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.SEND_SMS;

public class ScrollingActivity extends AppCompatActivity {

    private static final int REQUEST_SMS = 0;
    private boolean haveClick = false;
    private final User currentUser = User.instance;
    private Meal meal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        meal = getIntent().getParcelableExtra("meal");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(meal.getTitle());
        //TODO get Parcelable object and build the scroll page
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!haveClick) {
                    Snackbar.make(view, "Request send to the host", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    haveClick = true;

                    if (ContextCompat.checkSelfPermission(getApplicationContext(), SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(ScrollingActivity.this, SEND_SMS)) {
                            Snackbar.make(findViewById(R.id.app_bar), "Permission to send message from your phone to the host.", Snackbar.LENGTH_INDEFINITE)
                                .setAction(android.R.string.ok, new View.OnClickListener() {
                                    @Override
                                    @TargetApi(Build.VERSION_CODES.M)
                                    public void onClick(View v) {
                                        requestPermissions(new String[]{SEND_SMS}, REQUEST_SMS);
                                    }
                                });
                        } else {
                            ActivityCompat.requestPermissions(ScrollingActivity.this ,new String[]{SEND_SMS},REQUEST_SMS);
                        }
                    } else {
                        /*
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(currentUser.getPhoneNumber(), null, "Can " + currentUser.getDisplayName(getApplicationContext()) + " come to your awesome diner " + meal.getDate().toString() + "?", null, null);
                        */
                        sendSMS();
                    }
                } else {
                    Snackbar.make(view, "You already send a request. Wait for the respond", Snackbar.LENGTH_LONG).setAction("Action",null).show();
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        List<Ingredient> list = meal.getList();
        ListView listView = (ListView) findViewById(R.id.list_view_ingredient_scrolling);

        ScrollingIngredientAdapter adapter = new ScrollingIngredientAdapter(getApplicationContext(), list);
        listView.setAdapter(adapter);

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");

        TextView hostText = (TextView) findViewById(R.id.scrolling_host_text);
        hostText.setText("Meal is offor to you by " + meal.getHost().getDisplayName(this));
        TextView mealTitle = (TextView) findViewById(R.id.scrolling_meal_title);
        mealTitle.setText("Title : " + meal.getTitle() + "\nDescription : " + meal.getDescription());
        TextView mealTime = (TextView) findViewById(R.id.scrolling_meal_time);
        mealTime.setText("Time : " + sdf2.format(meal.getDate().getTime()));
        TextView mealDate = (TextView) findViewById(R.id.scrolling_meal_date);
        mealDate.setText("Date : " + sdf1.format(meal.getDate().getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.scrolling_menu_contact) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Information about the host");
            builder.setMessage("Name : " + meal.getHost().getDisplayName(this)
                + "\nEmail address : " + meal.getHost().getEmail()
                + "\nPhone number : " + meal.getHost().getPhoneNumber());
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            Toast.makeText(this,"Click on " + item.getTitle().toString(), Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_SMS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /*
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(currentUser.getPhoneNumber(), null, "Can " + currentUser.getDisplayName(getApplicationContext()) + " come to your awesome diner " + meal.getDate().toString() + "?", null, null);
                    */
                    sendSMS();
                    Toast.makeText(getApplicationContext(), "SMS sent.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                }
            }
        }

    }


    protected void sendSMS() {
        Log.i("Send SMS", "");
        Intent smsIntent = new Intent(Intent.ACTION_VIEW,Uri.parse("sms:" + currentUser.getPhoneNumber()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("sms_body"  , "Can " + currentUser.getDisplayName(getApplicationContext())
                + " come to your awesome diner " + sdf.format(meal.getDate().getTime())+ "?");
        try {
            startActivity(smsIntent);
            finish();
            Log.i("Finished sending SMS...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ScrollingActivity.this,
                    "SMS faild, please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ScrollingActivity.this,MapsActivity.class));
    }
}
