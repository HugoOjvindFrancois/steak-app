package com.example.hugo.steakapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.hugo.steakapp.adapter.IngredientAdapter;
import com.example.hugo.steakapp.data.MealList;
import com.example.hugo.steakapp.dummy.Ingredient;
import com.example.hugo.steakapp.model.Meal;
import com.example.hugo.steakapp.model.User;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class MealActivity extends AppCompatActivity {

    private final User currentUser = User.instance;

    private static RecyclerView rv;

    private boolean wasAtEnd = false;

    private int year, month, day, hour, minute;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private TabLayout tabLayout;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        setContentView(R.layout.activity_meal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            if(i == 0){
                tabLayout.getTabAt(i).setIcon(R.drawable.ic_assignment_24dp);
                tabLayout.getTabAt(i).getIcon().setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_IN);
            }
            if(i == 1){
                tabLayout.getTabAt(i).setIcon(R.drawable.ic_accessibility_24dp);
                tabLayout.getTabAt(i).getIcon().setColorFilter(Color.parseColor("#a8a8a8"), PorterDuff.Mode.SRC_IN);
            }
            if(i == 2){
                tabLayout.getTabAt(i).setIcon(R.drawable.ic_edit_location_24dp);
                tabLayout.getTabAt(i).getIcon().setColorFilter(Color.parseColor("#a8a8a8"), PorterDuff.Mode.SRC_IN);
            }
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_IN);
                floatingStepperHandler(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#a8a8a8"), PorterDuff.Mode.SRC_IN);
                floatingStepperHandler(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    public void floatingStepperHandler(TabLayout.Tab tab){
        if(tab.getPosition() == 2 && !wasAtEnd){
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),android.R.anim.fade_out);
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_slide);
            fab.startAnimation(animation);
            fab.setVisibility(View.GONE);
            wasAtEnd = true;
        } else if(wasAtEnd){
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),android.R.anim.fade_in);
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_slide);
            fab.setVisibility(View.VISIBLE);
            fab.startAnimation(animation);
            wasAtEnd = false;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(MealActivity.this, MainActivity.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_meal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MealActivity.this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {


        private List<Ingredient> ingredientList;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_LAYOUT_ID = "layout_id";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            int layoutId = R.layout.fragment_meal_ingredients;
            switch (sectionNumber){
                case 1 : layoutId = R.layout.fragment_meal_ingredients;break;
                case 2 : layoutId = R.layout.fragment_meal_scope;break;
                case 3 : layoutId = R.layout.fragment_meal_localisation;break;
            }
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putInt(ARG_LAYOUT_ID, layoutId);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View layoutInflater = inflater.inflate(getArguments().getInt(ARG_LAYOUT_ID), container, false);
            return layoutInflater;
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            switch(getArguments().getInt(ARG_SECTION_NUMBER)){
                case 1 :
                    rv = (RecyclerView)getView().findViewById(R.id.list_ingredient_view);
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    rv.setLayoutManager(llm);
                    final List<Ingredient> ingredients = new ArrayList<>();
                    IngredientAdapter adapter = new IngredientAdapter(getContext(),ingredients);
                    rv.setAdapter(adapter);
                    break;
            }
        }

    }


    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.tab_title_title_1);
                case 1:
                    return getResources().getString(R.string.tab_title_title_2);
                case 2:
                    return getResources().getString(R.string.tab_title_title_3);
            }
            return null;
        }
    }

    public void createButtonOnClick(View v){
        boolean cancel = false;

        EditText titleText = (EditText) findViewById(R.id.titre);
        EditText descriptionText = (EditText) findViewById(R.id.description);
        EditText peopleNumber = (EditText) findViewById(R.id.meal_people);

        String title = titleText.getText().toString();
        String description = descriptionText.getText().toString();
        if (Objects.equals(title, "")) {
            cancel = true;
            titleText.setError("Required");
        } if (Objects.equals(description, "")) {
            cancel = true;
            descriptionText.setError("Required");
        }

        int people = 0;
        if(!Objects.equals(peopleNumber.getText().toString(), ""))
            people = Integer.parseInt(peopleNumber.getText().toString());
        else {
            cancel = true;
            peopleNumber.setError("Required");
        }

        EditText longitudeText = (EditText) findViewById(R.id.longitude);
        EditText latitudeText = (EditText) findViewById(R.id.latitude);

        Double longitude = 0.;
        Double latitude = 0.;

        if(!Objects.equals(longitudeText.getText().toString(), "")) {
            longitude = Double.parseDouble(longitudeText.getText().toString());
            if (-180 >= longitude || longitude >= 180) {
                longitudeText.setError("Must be between -180 & 180 !");
                return;
            }
        } else {
            cancel = true;
            longitudeText.setError("Required");
        }

        if(!Objects.equals(latitudeText.getText().toString(), "")) {
            latitude = Double.parseDouble(latitudeText.getText().toString());
            if (-85.05115 >= latitude || latitude >= 85) {
                latitudeText.setError("Must be between -85 & 85 !");
                return;
            }
        } else {
            cancel = true;
            latitudeText.setError("Required");
        }

        List<Ingredient> list = new ArrayList<>();
        IngredientAdapter adapter = (IngredientAdapter) rv.getAdapter();

        if (adapter.getItemCount() > 0) {
            for (int i = 0; i < adapter.getItemCount(); i++) {
                list.add(adapter.get(i));
            }
        } else {
            cancel = true;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error !");
            builder.setMessage("You must add at lest one ingredient to your meal !");
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        if(!cancel) {
            MealList m = MealList.Instance;
            m.add(new Meal(title, currentUser, new LatLng(latitude, longitude), description, BitmapDescriptorFactory.HUE_RED, year, month, day, hour, minute, list, people));
            onBackPressed();
        }
    }

    public void createIngredientOnClick(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.ingredient_add, null);
        builder.setPositiveButton("Add new", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                boolean cancel = false;
                Dialog d = (Dialog) dialog;
                RecyclerView mRv = (RecyclerView) findViewById(R.id.list_ingredient_view);
                IngredientAdapter mAdapter = (IngredientAdapter) mRv.getAdapter();
                EditText name = (EditText) d.findViewById(R.id.name);
                String ingredientName = name.getText().toString();
                if (Objects.equals(ingredientName, "")) {
                    name.setError("Required");
                    cancel = true;
                }
                EditText quantity = (EditText) d.findViewById(R.id.quantity);
                int ingredientQuantity = 0;
                if (!Objects.equals(quantity.getText().toString(), ""))
                    ingredientQuantity = Integer.parseInt(quantity.getText().toString());
                else {
                    quantity.setError("Required");
                    cancel = true;
                }

                if (!cancel) {
                    Ingredient newIngredient = new Ingredient(ingredientName, ingredientQuantity, R.drawable.ic_add_circle_black_24dp, R.drawable.ic_remove_circle_24dp, R.drawable.ic_delete_24dp);
                    mAdapter.addItem(newIngredient, mAdapter.getItemCount());
                }
            }
        });
        builder.setNegativeButton("Cancel",null);
        builder.setTitle("Add ingredient");
        builder.setView(view);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void fabSlide(View v){
        int nextPage = mViewPager.getCurrentItem() == mViewPager.getChildCount() ? mViewPager.getCurrentItem() : mViewPager.getCurrentItem() + 1;
        mViewPager.setCurrentItem(nextPage,true);
    }

    public void chooseTime(View v){
        Calendar calendar = Calendar.getInstance();
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        int currentMinute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfDay) {
                        hour = hourOfDay;
                        minute = minuteOfDay;
                    }
                }, currentHour, currentMinute, false);
        timePickerDialog.show();
    }

    public void chooseDate(View v){
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int currentMonth = calendar.get(Calendar.MONTH);
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int totalYear, int monthOfYear, int dayOfMonth) {
                        year = totalYear;
                        month = monthOfYear;
                        day = dayOfMonth;
                    }
                }, currentYear, currentMonth, currentDay);
        datePickerDialog.show();
    }

}
