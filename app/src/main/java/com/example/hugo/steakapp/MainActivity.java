package com.example.hugo.steakapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hugo.steakapp.model.User;
import com.example.hugo.steakapp.transform.CircleTransform;
import com.example.hugo.steakapp.util.MD5Util;
import com.github.clans.fab.FloatingActionMenu;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int NOTIFICATION_ID = 1;
    private static final int REQUEST_CODE = 2;
    private final User currentUser = User.instance;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        //FloatingActionMenu fab = (FloatingActionMenu) findViewById(R.id.fab);
        //fab.setIconToggleAnimatorSet((AnimatorSet) AnimatorInflater.loadAnimator(getBaseContext(), R.animator.fab_menu));

        findViewById(R.id.fab_menu_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,MealActivity.class));
            }
        });

        findViewById(R.id.fab_menu_invite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,ItemListActivity.class));
            }
        });

        findViewById(R.id.fab_menu_setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,SettingsActivity.class));
            }
        });

        findViewById(R.id.fab_menu_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,MapsActivity.class));
            }
        });

        findViewById(R.id.fab_menu_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ForumActivity.class));
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        RelativeLayout navigationHeaderLayout = (RelativeLayout) navigationView.getHeaderView(0);
        ImageView profileImage = (ImageView) navigationHeaderLayout.findViewById(R.id.navigation_header_image);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });

        selectDrawerItem(1);


        ((TextView)navigationHeaderLayout.findViewById(R.id.navigation_header_name)).setText(currentUser.getDisplayName(getBaseContext()));
        ((TextView)navigationHeaderLayout.findViewById(R.id.navigation_header_email)).setText(currentUser.getEmail());

        Uri uri = currentUser.getImageURI();
        Picasso.with(this)
                .load(uri)
                .resizeDimen(R.dimen.cast_mini_controller_icon_width,R.dimen.cast_mini_controller_icon_height)
                .transform(new CircleTransform())
                .centerCrop()
                .into(profileImage);

        long[] tab = {5,5,5,5,5,5};

        final NotificationManager mNotification = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        final Intent launchNotifiactionIntent = new Intent(getApplicationContext(), MainActivity.class);
        final PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),REQUEST_CODE, launchNotifiactionIntent,PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.ic_cake_24dp))
                .setColor(getResources().getColor(R.color.primary,null))
                .setVibrate(tab)
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis())
                .setTicker("WUB A DUB DUB")
                //.setSmallIcon(R.drawable.ic_audiotrack)
                .setContentTitle(getResources().getString(R.string.notification_title))
                .setContentText(getResources().getString(R.string.notification_desc))
                .setContentIntent(pendingIntent)
                .addAction(new NotificationCompat.Action.Builder(R.drawable.ic_play_arrow_24dp,"Play", PendingIntent.getActivity(getApplicationContext(), 0,getIntent(), 0, null)).build())
                .addAction(new NotificationCompat.Action.Builder(R.drawable.ic_pause_24dp, "Pause",PendingIntent.getActivity(getApplicationContext(), 0, getIntent(), 0, null)).build());

        Notification notification = new NotificationCompat.BigPictureStyle(builder)
                .bigPicture(BitmapFactory.decodeResource(getResources(),R.drawable.polygone_background)).build();


        //mNotification.notify(NOTIFICATION_ID, notification);

        //StyleableToast.makeText(getBaseContext(), "Saving profile", Toast.LENGTH_SHORT, R.style.StyledToast).show();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        FloatingActionMenu floatingActionMenu = (FloatingActionMenu) findViewById(R.id.fab);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if(floatingActionMenu.isOpened()){
            floatingActionMenu.close(true);
        } else {
            moveTaskToBack(true);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        //TODO searchView return null, need to change it

        //MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        //View view = searchMenuItem.getActionView();

        //SearchView searchView = (SearchView) view;

        //MenuItem searchItem = menu.findItem(R.id.action_search);
        //SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        //searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        } else if(id == R.id.action_add){
            startActivity(new Intent(MainActivity.this, MealActivity.class));
        } else if (id == R.id.action_map) {
            startActivity(new Intent(MainActivity.this, MapsActivity.class));
        } else if (id == R.id.action_chat) {
            startActivity(new Intent(MainActivity.this, ForumActivity.class));
        } else if (id == R.id.action_contact) {
            startActivity(new Intent(MainActivity.this, ItemListActivity.class));
        } else if (id == R.id.action_shutdown) {
            MainActivity.this.finish();
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        } else {
            Toast.makeText(this,"Click on " + item.getTitle().toString(), Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            selectDrawerItem(1);
        } else if (id == R.id.nav_camera) {
            selectDrawerItem(2);
        } else if (id == R.id.nav_gallery) {
            selectDrawerItem(3);
        } else if (id == R.id.nav_slideshow) {
            selectDrawerItem(4);
        } else if (id == R.id.nav_manage) {
            selectDrawerItem(5);/*
        } else if (id == R.id.nav_switch){
            View actionView = MenuItemCompat.getActionView(item);
            actionView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(),"Vous avez switcher, LOL !",Toast.LENGTH_SHORT).show();
                }
            });*/
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        } else if (id == R.id.nav_add) {
            startActivity(new Intent(MainActivity.this, MealActivity.class));
        } else if (id == R.id.nav_map) {
            startActivity(new Intent(MainActivity.this, MapsActivity.class));
        } else if (id == R.id.nav_chat) {
            startActivity(new Intent(MainActivity.this, ForumActivity.class));
        } else if (id == R.id.nav_send) {
            startActivity(new Intent(MainActivity.this, ItemListActivity.class));
        } else if (id == R.id.nav_logout){
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.ic_info_black_24dp)
                    .setTitle("Logout")
                    .setMessage("Are you sure you want to disconnect ?")
                    .setPositiveButton("Bitch I'm out !", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.this.finish();
                            startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        }

                    })
                    .setNegativeButton("Please keep me !", null)
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        //TODO Don't work
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }

    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_LAYOUT_ID = "layout_id";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static MainActivity.PlaceholderFragment newInstance(int sectionNumber) {
            int layoutId;
            switch (sectionNumber){
                case 1 : layoutId = R.layout.fragment_main_home;break;
                case 2 : layoutId = R.layout.fragment_main_camera;break;
                case 3 : layoutId = R.layout.fragment_main_gallery;break;
                case 4 : layoutId = R.layout.fragment_main_slideshow;break;
                case 5 : layoutId = R.layout.fragment_main_manage;break;
                default: layoutId = R.layout.fragment_main_home;
            }
            MainActivity.PlaceholderFragment fragment = new MainActivity.PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putInt(ARG_LAYOUT_ID, layoutId);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
            View rootView = inflater.inflate(getArguments().getInt(ARG_LAYOUT_ID), container, false);
            return rootView;
        }
    }

    public void selectDrawerItem(int order) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;

        try {
            fragment = PlaceholderFragment.newInstance(order);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

    }


    public Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }



}
