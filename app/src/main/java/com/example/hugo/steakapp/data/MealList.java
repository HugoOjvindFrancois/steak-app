package com.example.hugo.steakapp.data;

import com.example.hugo.steakapp.model.Meal;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by LORD_OF_COMPSCI on 3/8/2017.
 */

public class MealList extends ArrayList<Meal> {

    public static MealList Instance = new MealList();

    public static Meal findMealByMarker(Marker marker){
        Meal meal = null;
        for(Meal m : Instance){
            if(Objects.equals(m.getTitle(), marker.getTitle())){
                if(Objects.equals(m.getDescription(), marker.getSnippet())){
                    if(m.getCoord().equals(marker.getPosition())){
                        meal = m;
                    }
                }
            }
        }
        return meal;
    }

    private MealList(){
        super();
    }
}
