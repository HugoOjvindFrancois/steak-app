package com.example.hugo.steakapp.dummy;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by LORD_OF_COMPSCI on 3/7/2017.
 */

public class Ingredient implements Parcelable{
    public String name;
    public int quantity;
    public int imageIdAdd;
    public int imageIdRemove;
    public int imageIdDelete;

    public Ingredient(String name, int quantite, int imageIdAdd, int imageIdRemove, int imageIdDelete){
        this.name = name;
        this.quantity = quantite;
        this.imageIdAdd = imageIdAdd;
        this.imageIdRemove = imageIdRemove;
        this.imageIdDelete = imageIdDelete;
    }

    protected Ingredient(Parcel in) {
        name = in.readString();
        quantity = in.readInt();
        imageIdAdd = in.readInt();
        imageIdRemove = in.readInt();
        imageIdDelete = in.readInt();
    }

    public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
        @Override
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        @Override
        public Ingredient[] newArray(int size) {
            return new Ingredient[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(quantity);
        dest.writeInt(imageIdAdd);
        dest.writeInt(imageIdRemove);
        dest.writeInt(imageIdDelete);
    }

    public String getName(){
        return  this.name;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity(){
        return this.quantity;
    }
}
