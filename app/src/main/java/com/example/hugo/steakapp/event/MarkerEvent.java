package com.example.hugo.steakapp.event;

import com.example.hugo.steakapp.model.Meal;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Hugo on 23/02/2017.
 */

public class MarkerEvent {

    private Meal meal;

    public MarkerEvent(Meal meal){
    }

    public LatLng getCoord(){
        return this.meal.getCoord();
    }

    public String getTitle(){
        return this.meal.getTitle();
    }

    public String getSnippet() {
        return meal.getDescription();
    }

    public float getColor() {
        return meal.getColor();
    }
}
