package com.example.hugo.steakapp.factory;

import com.example.hugo.steakapp.dummy.Ingredient;
import com.example.hugo.steakapp.event.MarkerEvent;
import com.example.hugo.steakapp.model.Meal;
import com.example.hugo.steakapp.model.User;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Hugo on 23/02/2017.
 */

public class MealFactory extends Observable {

    public List<Meal> mealList = new ArrayList<>();

    private static MealFactory mealFactory;

    public static MealFactory getInstance(){
        if(mealFactory != null) return mealFactory;
        else return new MealFactory();
    }

    private MealOption builder;

    private MealFactory(){}

    public Meal buildMeal(double x, double y, String title, String description, float color, int year, int month, int day, int hour, int minute, User host, List<Ingredient> list, int people){
        LatLng coor = new LatLng(x,y);
        Meal meal = new Meal(title, host, coor, description, color, year, month, day, hour, minute, list, people);
        notifyObservers(new MarkerEvent(meal));
        this.mealList.add(meal);
        return meal;
    }

    public MealOption builder(){
        return builder;
    }

    public void addMealObeserver(Observer observer){
        addObserver(observer);
    }

}
