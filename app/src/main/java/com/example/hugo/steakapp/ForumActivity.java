package com.example.hugo.steakapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hugo.steakapp.adapter.ForumThreadAdapter;
import com.example.hugo.steakapp.dummy.ForumThread;

import java.util.ArrayList;
import java.util.List;

public class ForumActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    selectDrawerItem(1);
                    return true;
                case R.id.navigation_dashboard:
                    selectDrawerItem(2);
                    return true;
                case R.id.navigation_notifications:
                    selectDrawerItem(3);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        setContentView(R.layout.activity_forum);


        selectDrawerItem(1);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ForumActivity.this, MainActivity.class));
    }


    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_LAYOUT_ID = "layout_id";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ForumActivity.PlaceholderFragment newInstance(int sectionNumber) {
            int layoutId;
            switch (sectionNumber){
                case 1 : layoutId = R.layout.fragment_forum_home;break;
                case 2 : layoutId = R.layout.fragment_forum_dashboard;break;
                case 3 : layoutId = R.layout.fragment_forum_notification;break;
                default: layoutId = R.layout.fragment_forum_home;
            }
            ForumActivity.PlaceholderFragment fragment = new ForumActivity.PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putInt(ARG_LAYOUT_ID, layoutId);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(getArguments().getInt(ARG_LAYOUT_ID), container, false);
            return rootView;
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            switch(getArguments().getInt(ARG_SECTION_NUMBER)){
                case 2 :List<ForumThread> list = new ArrayList<>();
                        for(int i = 1; i <= 100; i++)
                            list.add(new ForumThread("Wub a " + i + " dub dub !"));
                        ListView listView = (ListView) getView().findViewById(R.id.forum_thread_list);
                        ForumThreadAdapter adapter = new ForumThreadAdapter(list);
                        listView.setAdapter(adapter);
                        break;
            }
        }
    }

    public void selectDrawerItem(int order) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;

        try {
            fragment = ForumActivity.PlaceholderFragment.newInstance(order);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();

    }

}
